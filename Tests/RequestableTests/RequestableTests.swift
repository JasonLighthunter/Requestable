import XCTest
@testable import Requestable

class Person: Requestable {
  static var requestURLString: String = "person"
}

class RequestableTests: XCTestCase {
  func testRequestable() {
    XCTAssertEqual(Person.requestURLString, "person")
  }
}
