import XCTest
@testable import RequestableTests

XCTMain([
    testCase(RequestableTests.allTests)
])
