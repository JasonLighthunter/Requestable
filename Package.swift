// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "Requestable",
  products: [
    .library(name: "Requestable", targets: ["Requestable"])
  ],
  dependencies: [],
  targets: [
    .target(name: "Requestable", dependencies: []),
    .testTarget( name: "RequestableTests", dependencies: ["Requestable"])
  ]
)
