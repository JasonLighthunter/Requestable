public protocol Requestable {
    static var requestURLString: String {get}
}
